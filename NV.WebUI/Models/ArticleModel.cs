﻿using NV.Entities;
using NV.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NV.WebUI.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }
        [Display(Name="Рубрика")]
        public string Rubric { get; set; }
        [Required(ErrorMessage="Заголовок не може бути порожнім")]
        [Display(Name = "Заголовок")]
        public string Header { get; set; }
        [Required(ErrorMessage = "Вступ не може бути порожнім")]
        [Display(Name = "Вступ")]
        public string Intro { get; set; }
        [Required(ErrorMessage = "Основний текст статті не може бути порожнім")]
        [Display(Name = "Основний текст")]
        public string Body { get; set; }        
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Категорія не вказана")]
        [Display(Name = "Категорія")]
        public string Category { get; set; }
        [Required(ErrorMessage="Автора не вказано")]
        [Display(Name="Автор")]
        public int AuthorId { get; set; }

        public static implicit operator ArticleModel(Article a)
        {
            var model = new ArticleModel()
            {
                Id = a.Id,
                Rubric = a.Rubric,
                Header = a.Header,
                Intro = a.Intro,
                Body = a.Body,
                Date = a.Date,
                Category = a.Category,
                AuthorId = a.AuthorId,
            };
            return model;
        }

        public static implicit operator Article(ArticleModel m)
        {
            var entity = new Article()
            {
                Id = m.Id,
                Rubric = m.Rubric,
                Header = m.Header,
                Intro = m.Intro,
                Body = m.Body,
                Date = m.Date,
                Category = m.Category,
                AuthorId = m.AuthorId                
            };
            return entity;
        }

        public List<SelectListItem> DropDownList(IAuthorRepository repository)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var a in repository.Authors)
            {
                list.Add(new SelectListItem() { Value = a.Id.ToString(), Text = a.Name + " " + a.Surname });
            }
            return list;
        }
    }
}