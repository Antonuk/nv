﻿using System.ComponentModel.DataAnnotations;

namespace NV.WebUI.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage="Email can not be empty")]
        [StringLength(128, ErrorMessage = "Your name is too long")]
        [DataType(DataType.Text)]
        [Display(Name="Email")]
        public string Name { get; set; }

        [Display(Name="Password")]
        [Required(ErrorMessage="Password can not be empty")]
        [MinLength(4, ErrorMessage= "The minimum length of the password must be 8 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}