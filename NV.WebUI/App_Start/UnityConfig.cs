using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using NV.Repositories.Abstract;
using System.Configuration;
using NV.Repositories.Sql;
using NV.WebUI.Code.Manager;
using NV.Repositories.Fake;

namespace NV.WebUI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            string connectionString = ConfigurationManager.ConnectionStrings["NvDb"].ConnectionString;  
          
            container.RegisterType<IArticleRepository, SqlArticleRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<IAuthorRepository, SqlAuthorRepository>(new InjectionConstructor(connectionString));            
            container.RegisterType<IArticleEntityRepository, SqlArticleEntityRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ISecurityManager, FormsSecurityManager>();
            container.RegisterType<IUserRepository, FakeAdminRepository>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}