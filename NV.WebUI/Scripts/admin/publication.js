﻿(function() {
    
    var onSelectFilter = function (e) {
        $el = $(e.target);
        var filter = $el.find("option:selected").attr("value");
        console.log("Filter = " + filter);
        $.ajax({
            type: "get",
            url: "/Admin/Publication/FilterByCategory",
            dataType: "html",
            data: {
                filter : e.target.value
            }
        }).done(function (data) {
            console.log("ajax filter by " + e.target.value + " is done!");
            $("#list").html(data);
        }).fail(function () {
            console.log("ajax filter is FAIL");
        })
    };

    var initStatusFilter = function () {
        $("#selecter").on("change", onSelectFilter);
        console.log("Status filter init is fine");
    };

    $(function () {
        initStatusFilter();
        console.log("publication.js is loaded");
    });
}) ();