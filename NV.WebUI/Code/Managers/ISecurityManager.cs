﻿namespace NV.WebUI.Code.Manager
{
    public interface ISecurityManager
    {
        bool Login(string email, string password);
        void Logout();
        bool IsAuthentificated();
    }
}