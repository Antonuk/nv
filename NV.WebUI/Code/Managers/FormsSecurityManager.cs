﻿using NV.Repositories.Abstract;
using System.Web;
using System.Web.Security;

namespace NV.WebUI.Code.Manager
{
    public class FormsSecurityManager : ISecurityManager
    {
        public readonly IUserRepository _repository;

        public FormsSecurityManager(IUserRepository repository)
        {
            this._repository = repository;
        }

        public bool Login(string email, string password)
        {
            if(_repository.Login(email, password))
            {
                FormsAuthentication.SetAuthCookie(email, false);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsAuthentificated()
        {
            var user = HttpContext.Current.User;
            return (user != null) && (user.Identity != null) && (user.Identity.IsAuthenticated == true);
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}