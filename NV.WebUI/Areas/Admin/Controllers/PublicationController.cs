﻿using NV.Entities;
using NV.Repositories.Abstract;
using NV.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace NV.WebUI.Areas.Admin.Controllers
{
    public class PublicationController : Controller
    {
        private readonly IArticleEntityRepository _model;
        private readonly IArticleRepository _articleRepository;
        private readonly IAuthorRepository _authorRepository;
        public PublicationController(IArticleEntityRepository repository, IArticleRepository articles, 
            IAuthorRepository authors)
        {
            this._model = repository;
            this._articleRepository = articles;
            this._authorRepository = authors;
            ViewBag.Categories = _model.Categories();
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Articles = _model.GetAll();
            return View();
        }
        
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ArticleModel articleModel = _model.GetById(id).Article;
            ViewBag.authors = articleModel.DropDownList(_authorRepository);
            return View(articleModel);
        }

        [HttpGet]
        public ViewResult Add()
        {
            var articleModel = new ArticleModel();
            ViewBag.authors = articleModel.DropDownList(_authorRepository);
            return View("Edit", articleModel);
        }

        [HttpPost]
        public ActionResult Edit(ArticleModel articleModel)
        {
            if (ModelState.IsValid)
            {
                Article entity = articleModel;
                if(entity.Id == 0)
                {
                    _articleRepository.Add(entity);
                }
                else
                {
                    _articleRepository.Edit(entity);
                }
                TempData["message"] = string.Format("Changes is the game \"{0}\" was saved", articleModel.Header);                
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Please fill all required fields");
                return View(articleModel);
            }
        }

        [HttpGet]
        public ActionResult FilterByCategory(string filter = null)
        {
            if(filter == "All")
            {
                filter = null;
            }
            return PartialView("ArticlesListPartial", _model.FilterByCategory(filter));
        }

        [HttpPost]
        public ActionResult Delete(int articleId)
        {
            _articleRepository.Delete(articleId);
            TempData["message"] = string.Format("Article #\"{0}\" was deleted", articleId);
            return RedirectToAction("Index");
        }
    }
}