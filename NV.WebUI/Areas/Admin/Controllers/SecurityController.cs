﻿using NV.WebUI.Code.Manager;
using NV.WebUI.Models;
using System.Web.Mvc;

namespace NV.WebUI.Areas.Admin.Controllers
{
    public class SecurityController : Controller
    {
        private ISecurityManager _manager;
        public SecurityController(ISecurityManager manager)
        {
            this._manager = manager;
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (_manager.IsAuthentificated())
            {
                return RedirectToAction("Index", "Task");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            if (_manager.Login(loginModel.Name, loginModel.Password))
            {
                return RedirectToAction("Index", "Publication");
            }
            else
            {                
                if (loginModel.Name != "admil@mail.loc")
                {
                    ModelState.AddModelError("", "Email doesn't match");
                }
                if (loginModel.Password != "admin")
                {
                    ModelState.AddModelError("", "Password doesn't match");
                }
                return View();
            }
        }

        [HttpPost]
        public ActionResult Logout()
        {
            _manager.Logout();
            return RedirectToAction("Login", "Security");
        }
    }
}