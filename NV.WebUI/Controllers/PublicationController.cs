﻿using NV.Repositories.Abstract;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Collections;

namespace NV.WebUI.Controllers
{
    public class PublicationController : Controller
    {      
        private readonly IArticleEntityRepository _model;      
        public PublicationController(IArticleEntityRepository repository)
        {
            this._model = repository;
            InitCategoryList();
        }

        [HttpGet]
        public async Task<ActionResult> Index(string category = null)
        {
            ViewBag.Selected = category;
            ViewBag.Articles = await _model.GetAllWithTask(category);                      
            return  View();
        }
        
        [HttpGet]
        public ActionResult InitCategoryList()
        {
            IEnumerable<string> categories = _model.Categories();
            ViewBag.Categories = categories;
            return PartialView("SubMenuPanel", ViewBag.Categories);
        }

        [HttpGet]
        public ActionResult Article(int id)
        {
            var article = _model.GetAll().ToList().Find(x => x.Article.Id == id);
            return View(article);
        }
    }
}