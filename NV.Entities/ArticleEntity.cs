﻿namespace NV.Entities
{
    public class ArticleEntity
    {
        public Article Article { get; set; }
        public Author Author { get; set; }
    }
}
