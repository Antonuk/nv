﻿using System;
using System.Text;

namespace NV.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string Rubric { get; set; }
        public string Header { get; set; }
        public string Intro { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set; }
        public string Category { get; set; }
        public int AuthorId { get; set; }

    }
}