﻿using NV.Entities;
using System.Data.Entity;

namespace NV.Repositories.Sql
{
    public class ArticleDbContext : DbContext
    {
        public ArticleDbContext(string connectionString) : base(connectionString) { }
        public DbSet<Article> Articles { get; set; }
    }
}