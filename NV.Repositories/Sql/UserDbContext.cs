﻿using NV.Entities;
using System.Data.Entity;

namespace NV.Repositories.Sql
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(string connectionString) : base(connectionString) { }
        public DbSet<User> Users { get; set; }
    }
}
