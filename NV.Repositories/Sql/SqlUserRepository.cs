﻿using NV.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using NV.Entities;

namespace NV.Repositories.Sql
{
    public class SqlUserRepository : IUserRepository
    {
        private string _connectionString;
        private UserDbContext _context;
        public SqlUserRepository(string connectionString)
        {
            this._connectionString = connectionString;
            this._context = new UserDbContext(_connectionString);
        }
        private IEnumerable<User> Users
        {
            get { return _context.Users; }
        }
        public User FindByEmail(string email)
        {
            return Users.Where(x => x.Email == email).FirstOrDefault();
        }

        public bool Login(string email, string password)
        {
            var user = FindByEmail(email);
            if(user == null)
            {
                throw new ArgumentNullException("User with this email doesn't exist");
            }
            return user.Password == password;
        }
    }
}
