﻿using NV.Repositories.Abstract;
using System.Collections.Generic;
using NV.Entities;

namespace NV.Repositories.Sql
{
    public class SqlAuthorRepository : IAuthorRepository
    {
        private readonly string _connectionString;
        private AuthorDbContext _context;
        public SqlAuthorRepository(string cs)
        {
            this._connectionString = cs;
            _context = new AuthorDbContext(_connectionString);
        }
        public IEnumerable<Author> Authors
        {
            get
            {
                return _context.Authors;
            }
        }
    }
}