﻿using NV.Entities;
using System.Data.Entity;

namespace NV.Repositories.Sql
{
    public class AuthorDbContext : DbContext
    {
        public AuthorDbContext(string connectionString) : base(connectionString) { }
        public DbSet<Author> Authors { get; set; }
    }
}