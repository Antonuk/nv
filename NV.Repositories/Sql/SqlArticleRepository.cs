﻿using NV.Entities;
using NV.Repositories.Abstract;
using System.Collections.Generic;
using System;
using System.Linq;

namespace NV.Repositories.Sql
{
    public class SqlArticleRepository : IArticleRepository
    {
        private readonly string _connectionString;
        private ArticleDbContext _context;
        public SqlArticleRepository(string connectionString)
        {
            this._connectionString = connectionString;
            _context = new ArticleDbContext(_connectionString);
        }
        public IEnumerable<Article> Articles
        {
            get
            {
                //using (var context = new ArticleDbContext(_connectionString))
                //{
                    return _context.Articles;
                //}
            }
        }

        public void Delete(int id)
        {
            var entryEntity = Articles.Where(x => x.Id == id).First();
            if (entryEntity != null)
            {
                _context.Articles.Remove(entryEntity);
                _context.SaveChanges();
            }
        }

        public void Add(Article entry)
        {
            entry.Date = DateTime.Now;
            _context.Articles.Add(entry);
            _context.SaveChanges();
        }

        public void Edit(Article entry)
        {
            Article edited = _context.Articles.Where(x => x.Id == entry.Id).First();
            if(edited != null)
            {
                edited.Id = entry.Id;
                edited.Rubric = entry.Rubric;                
                edited.Header = entry.Header;
                edited.Intro = entry.Intro;
                edited.Body = entry.Body;
                edited.Category = entry.Category;
                edited.AuthorId = entry.AuthorId;
                //date don't need to edit
            }
            _context.SaveChanges();
        }
    }
}