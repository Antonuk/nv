﻿using NV.Repositories.Abstract;
using System.Collections.Generic;
using System.Linq;
using NV.Entities;
using System;
using System.Threading.Tasks;

namespace NV.Repositories.Sql
{
    public class SqlArticleEntityRepository : IArticleEntityRepository
    {
        private string _connectionString;
        private IEnumerable<ArticleEntity> _articleModels;
        private IAuthorRepository _authorsRepository;
        private IArticleRepository _articlesRepository;
        public SqlArticleEntityRepository(string connectionString)
        {
            this._connectionString = connectionString;
            this._authorsRepository = new SqlAuthorRepository(_connectionString);
            this._articlesRepository = new SqlArticleRepository(_connectionString);
            this._articleModels = GetAll();
        }

        public IEnumerable<string> Categories()
        {
            var list = from a in _articleModels
                       select a.Article.Category;
            return list.Distinct();
        }

        public IEnumerable<ArticleEntity> FilterByCategory(string category = null)
        {
            return (category == null) ? GetAll() : GetAll().Where(x => x.Article.Category == category);
        }

        public async Task<IEnumerable<ArticleEntity>> GetAllAsync(string category = null)
        {
            return  (category == null) ? await new Task<IEnumerable<ArticleEntity>>(GetAll)
                : await FilterAsync(category);        
        }


        public Task<IEnumerable<ArticleEntity>> GetAllWithTask(string category = null)
        {
            return Task.Run(
                    () =>
                    {
                        return (category == null) ? GetAll() 
                        : GetAll().Where(x => x.Article.Category == category);
                    }                
                );
        }


        //this way we wrote async methods
        private Task<IEnumerable<ArticleEntity>> FilterAsync(string filter)
        {
            return Task.Run(
                () =>
                {
                    return GetAll().Where(x => x.Article.Category == filter);
                });
        }

        public IEnumerable<ArticleEntity> GetAll()
        {
            List<ArticleEntity> articles = new List<ArticleEntity>();
            var res = from article in _articlesRepository.Articles
                      join author in _authorsRepository.Authors
                      on article.AuthorId equals author.Id
                      select new ArticleEntity
                      {
                          Article = new Article
                          {
                              Id = article.Id,
                              Rubric = article.Rubric,
                              Header = article.Header,
                              Intro = article.Intro,
                              Body = article.Body,
                              Date = article.Date,
                              AuthorId = article.AuthorId,
                              Category = article.Category
                          },
                          Author = new Author
                          {
                              Id = author.Id,
                              Name = author.Name,
                              Surname = author.Surname,
                              Info = author.Info
                          }
                      };
            foreach (var model in res)
            {
                articles.Add(model);
            }
            return articles;
        }

        public ArticleEntity GetById(int id)
        {
            return _articleModels.Where(x => x.Article.Id == id).First();
        }
    }
}