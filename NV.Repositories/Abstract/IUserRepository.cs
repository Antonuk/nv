﻿using NV.Entities;

namespace NV.Repositories.Abstract
{
    public interface IUserRepository
    {
        bool Login(string email, string password);
    }
}
