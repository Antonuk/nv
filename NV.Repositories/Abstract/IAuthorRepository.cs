﻿using NV.Entities;
using System.Collections.Generic;

namespace NV.Repositories.Abstract
{
    public interface IAuthorRepository
    {
        IEnumerable<Author> Authors { get; }
    }
}