﻿using NV.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NV.Repositories.Abstract
{
    public interface IArticleEntityRepository
    {
        IEnumerable<ArticleEntity> GetAll();
        IEnumerable<ArticleEntity> FilterByCategory(string category = null);
        IEnumerable<string> Categories();
        ArticleEntity GetById(int id);
        Task<IEnumerable<ArticleEntity>> GetAllWithTask(string category = null);
    }
}
