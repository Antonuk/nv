﻿using NV.Entities;
using System.Collections.Generic;

namespace NV.Repositories.Abstract
{
    public interface IArticleRepository
    {
        IEnumerable<Article> Articles { get; }
        void Delete(int id);
        void Add(Article entry);
        void Edit(Article entry);
    }
}