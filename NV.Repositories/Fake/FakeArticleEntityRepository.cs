﻿using NV.Entities;
using NV.Repositories.Abstract;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace NV.Repositories.Fake
{
    public class FakeArticleEntityRepository : IArticleEntityRepository
    {
        private IEnumerable<ArticleEntity> _articleModels;
        private IAuthorRepository _authors;
        private IArticleRepository _articles;
        public FakeArticleEntityRepository()
        {
            this._articles = new FakeArticleRepository();
            this._authors = new FakeAuthorRepository();
            this._articleModels = Create(_articles, _authors);
        }
        private IEnumerable<ArticleEntity> Create(IArticleRepository articles, IAuthorRepository authors)        
        {
            var articleModel = new List<ArticleEntity>();

            var res = from article in articles.Articles
                      join author in authors.Authors
                      on article.AuthorId equals author.Id                      
                      select new ArticleEntity
                      {
                          Article = new Article
                           {
                               Id = article.Id,
                               Rubric = article.Rubric,
                               Header = article.Header,
                               Intro = article.Intro,
                               Body = article.Body,
                               Date = article.Date,
                               Category = article.Category,
                               AuthorId = article.AuthorId                               
                           },
                          Author = new Author
                          {
                              Id = author.Id,
                              Name = author.Name,
                              Surname = author.Surname,
                              Info = author.Info
                          },
                      };
            foreach(var m in res)
            {
                articleModel.Add(m);
            }
            return articleModel;
        }

        public IEnumerable<ArticleEntity> FilterByCategory(string category = null)
        {
           return (category == null) ? GetAll() :
                 GetAll().Where(x => x.Article.Category == category);
        }

        public IEnumerable<string> Categories()
        {
            var tmp = from a in _articleModels
                      select a.Article.Category;
            return tmp;
        }

        public IEnumerable<ArticleEntity> GetAll()
        {
            return this._articleModels;
        }

        public ArticleEntity GetById(int id)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<dynamic> GetAllWithTask(string category)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<ArticleEntity>> IArticleEntityRepository.GetAllWithTask(string category)
        {
            throw new NotImplementedException();
        }
    }
}
