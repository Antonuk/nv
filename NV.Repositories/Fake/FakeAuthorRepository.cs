﻿using NV.Entities;
using NV.Repositories.Abstract;
using System.Collections.Generic;

namespace NV.Repositories.Fake
{
    public class FakeAuthorRepository : IAuthorRepository
    {
        private IEnumerable<Author> _authors;
        public FakeAuthorRepository()
        {
            this._authors = Create();
        }

        private IEnumerable<Author> Create()
        {
            var authorList = new List<Author>();
            authorList.Add(new Author { Id = 1, Name = "Roma", Surname = "Jolud", Info = "Boh" });
            authorList.Add(new Author { Id = 2, Name = "Kolya", Surname = "Ramzes", Info = null });
            authorList.Add(new Author { Id = 3, Name = "Vika", Surname = "Hot", Info = "Baby" });
            authorList.Add(new Author { Id = 4, Name = "Olya", Surname = "Bida", Info = null });
            return authorList;
        }
        public IEnumerable<Author> Authors
        {
            get { return this._authors; }
        }
    }
}
