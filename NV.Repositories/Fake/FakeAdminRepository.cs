﻿using NV.Repositories.Abstract;

namespace NV.Repositories.Fake
{
    public class FakeAdminRepository : IUserRepository
    {
        private readonly string _email = "admin@mail.loc";
        private readonly string _password = "admin";

        public bool Login(string email, string password)
        {
            return (this._email == email && this._password == password) ? true : false;
        }
    }
}
