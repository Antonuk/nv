﻿using NV.Entities;
using NV.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NV.Repositories.Fake
{
    public class FakeArticleRepository : IArticleRepository
    {
        private IEnumerable<Article> _articles;
        public FakeArticleRepository()
        {
            this._articles = Create();
        }
        public IEnumerable<Article> Articles
        {
            get { return this._articles; }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<Article> Create()
        {
            List<Article> articleList = new List<Article>();
            articleList.Add(new Article { Id = 1, Rubric = "Rubric1", Header = "Header1",
                Intro = "Intro1", Body = "Body1", Date = DateTime.Now, AuthorId = 1, Category = "Official"});
            articleList.Add(new Article { Id = 2, Rubric = "Rubric2", Header = "Header2",
                Intro = "Intro2", Body = "Body2", Date = DateTime.Now, AuthorId = 2, Category = "Sports" });
            articleList.Add(new Article { Id = 3, Rubric = "Rubric3", Header = "Header3",
               Intro = "Intro3", Body = "Body3", Date = DateTime.Now, AuthorId = 3, Category = "Medicine" });
            articleList.Add(new Article { Id = 4, Rubric = "Rubric4", Header = "Header4",
               Intro = "Intro4", Body = "Body4", Date = DateTime.Now, AuthorId = 2, Category = "Other" });
            return articleList;
        }


        public void Add(Article entry)
        {
            throw new NotImplementedException();
        }

        public void Edit(Article entry)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Article> IArticleRepository.Articles
        {
            get { throw new NotImplementedException(); }
        }

        void IArticleRepository.Delete(int id)
        {
            throw new NotImplementedException();
        }

        void IArticleRepository.Add(Article entry)
        {
            throw new NotImplementedException();
        }

        void IArticleRepository.Edit(Article entry)
        {
            throw new NotImplementedException();
        }
    }
}
