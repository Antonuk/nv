CREATE TABLE [dbo].[Authors]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Name] NVARCHAR(64) NOT NULL,
	[Surname] NVARCHAR(64) NOT NULL,
	[Info] NVARCHAR(128) NULL
	CONSTRAINT pk_author_id PRIMARY KEY([Id])
);
GO

CREATE TABLE [dbo].[Articles]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Rubric] NVARCHAR(64) NULL,
	[Header] NVARCHAR(64) NOT NULL,
	[Intro] NVARCHAR(1024) NOT NULL,
	[Body] NVARCHAR(MAX) NOT NULL,	
	[Date] DATE NOT NULL,
	[Category] NVARCHAR(64) NOT NULL,
	[AuthorId] INT NOT NULL,
	/*ImageList*/
	CONSTRAINT pk_article_id PRIMARY KEY([Id]),
	CONSTRAINT fk_article_authorId_author_id
	FOREIGN KEY ([AuthorId]) REFERENCES Authors([Id])
);
GO

/*
CREATE TABLE [dbo].[Users]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Name] NVARCHAR(64) NOT NULL,
	[Surname] NVARCHAR(64) NOT NULL,
	[Email] NVARCHAR(64) NOT NULL,
	[Password] NVARCHAR(512) NOT NULL
	CONSTRAINT pk_user_id PRIMARY KEY(Id)
)
GO
*/
SELECT * FROM Articles